package com.assignment.carparkingsystem.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkData;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkInfo;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.Item;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.util.CarParkDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.kafka.support.Acknowledgment;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class KafkaConsumerServiceImplTest {

  private KafkaConsumerServiceImpl kafkaConsumerServiceImpl;
  @Mock
  private Acknowledgment acknowledgment;
  @Mock
  private ConsumerRecord<String, String> consumerRecord;
  @Mock
  private CarParkService carParkService;
  @Mock
  private CarParkDataUtil carParkDataUtil;
  @Mock
  private ObjectMapper objectMapper;

  private CarParkAvailabilityApiResponse carParkAvailabilityApiResponse = new CarParkAvailabilityApiResponse();
  private List<CarPark> carParkList = new ArrayList<>();
  private CarPark carPark = new CarPark();
  private CarParkAvailabilityApiResponse.CarParkData carParkData = new CarParkData();


  @BeforeEach
  void setUp() {
    kafkaConsumerServiceImpl = new KafkaConsumerServiceImpl(carParkService, carParkDataUtil);

    carPark = new CarPark(UUID.randomUUID(), "BE24", "test", 1.0, 1.0, 1, 5,
        LocalDateTime.now(), LocalDateTime.now(), 1.0);
    carParkList.add(carPark);

    List<CarParkInfo> carParkInfos = new ArrayList<>();
    carParkInfos.add(new CarParkInfo(2, "C", 1));
    carParkData = new CarParkData(carPark.getCarParkNo(), LocalDateTime.now().toString(),
        carParkInfos);
    List<CarParkAvailabilityApiResponse.CarParkData> carParkDataList = new ArrayList<>();
    carParkDataList.add(carParkData);
    List<CarParkAvailabilityApiResponse.Item> items = new ArrayList<>();
    items.add(new Item(LocalDateTime.now().toString(), carParkDataList));
    carParkAvailabilityApiResponse.setItems(items);
  }

  @Test
  void testConsumeUpdateCarParkEvent_When_EventDTOIsValid_Then_CallExpectedMethods()
      throws JsonProcessingException {
    String carParkString = "{\"carpark_info\":[{\"total_lots\":\"583\",\"lot_type\":\"C\",\"lots_available\":\"246\"}],\"carpark_number\":\"BE24\",\"update_datetime\":\"2024-02-27T19:57:33\"}";
    doReturn(carParkString).when(consumerRecord).value();
    doReturn(carParkAvailabilityApiResponse).when(objectMapper)
        .readValue(anyString(), eq(CarParkAvailabilityApiResponse.class));
    doReturn(carPark).when(carParkService).findByCarParkNumber(carPark.getCarParkNo());
    doNothing().when(carParkService).updateCarParkAvailability(carPark, carParkData);
    kafkaConsumerServiceImpl.consumeUpdateCarParkEvent(consumerRecord, acknowledgment);
    verify(acknowledgment, times(1)).acknowledge();
  }
}
