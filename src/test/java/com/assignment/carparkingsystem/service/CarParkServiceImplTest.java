package com.assignment.carparkingsystem.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkData;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkInfo;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.Item;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.repository.CarParkRepository;
import com.assignment.carparkingsystem.util.CarParkDataUtil;
import com.assignment.carparkingsystem.util.CoordinateConversionUtility;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {CarParkServiceImpl.class})
@ExtendWith(SpringExtension.class)
class CarParkServiceImplTest {

    @MockBean
    private CarParkRepository carParkRepository;
    @MockBean
    private CoordinateConversionUtility coordinateConversionUtility;
    @MockBean
    private CarParkDataUtil carParkDataUtil;
    @Autowired
    private CarParkServiceImpl carParkService;

    private List<CarPark> carParkList = new ArrayList<>();
    private CarParkQueryDto carParkQueryDto = new CarParkQueryDto();
    private CarParkAvailabilityApiResponse carParkAvailabilityApiResponse = new CarParkAvailabilityApiResponse();
    private CarParkAvailabilityApiResponse.CarParkData carParkData = new CarParkData();
    private CarPark carPark = new CarPark();


    @BeforeEach
    void setUp() {
        carParkQueryDto = new CarParkQueryDto(1.0, 1.0, 1, 1);
        carPark = new CarPark(UUID.randomUUID(), "Test", "test", 1.0, 1.0, 1, 5,
            LocalDateTime.now(), LocalDateTime.now(), 1.0);
        carParkList.add(carPark);

        carPark = new CarPark(UUID.randomUUID(), "BE24", "test", 1.0, 1.0, 1, 5,
            LocalDateTime.now(), LocalDateTime.now(), 1.0);
        carParkList.add(carPark);

        List<CarParkInfo> carParkInfos = new ArrayList<>();
        carParkInfos.add(new CarParkInfo(2, "C", 1));
        carParkData = new CarParkData(carPark.getCarParkNo(), LocalDateTime.now().toString(),
            carParkInfos);
        List<CarParkAvailabilityApiResponse.CarParkData> carParkDataList = new ArrayList<>();
        carParkDataList.add(carParkData);
        List<CarParkAvailabilityApiResponse.Item> items = new ArrayList<>();
        items.add(new Item(LocalDateTime.now().toString(), carParkDataList));
        carParkAvailabilityApiResponse.setItems(items);
    }

    @Test
    void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_CarParkDtoList() {
        when(carParkRepository.findAll()).thenReturn(carParkList);
        when(coordinateConversionUtility.calculateHaversineDistance(anyDouble(), anyDouble(), anyDouble(),
            anyDouble())).thenReturn(1.0);
        when(coordinateConversionUtility.setDoublePrecision(anyDouble(), anyInt())).thenReturn(1.12345);
        CarParkDtoList carParkDtoList = carParkService.findNearestCarParks(carParkQueryDto);
        assertEquals(1, carParkDtoList.getCarParks().size());
        assertEquals(2, carParkDtoList.getTotalCarParks());
    }

    @Test
    void testGetNearestCarParks_When_AllQueriesAreValidAndReturnListIsEmpty_Then_EmptyCarParkDtoList() {
        when(carParkRepository.findByAvailableLotsNot(0)).thenReturn(new ArrayList<>());
        CarParkDtoList carParkDtoList = carParkService.findNearestCarParks(carParkQueryDto);
        assertEquals(0, carParkDtoList.getCarParks().size());
        assertEquals(0, carParkDtoList.getTotalCarParks());
    }

    @Test
    void testFindAllCarParks_Return_ListCarParkDto() {
        when(carParkRepository.findAll()).thenReturn(carParkList);
        List<CarPark> carParkDtoList = carParkService.findAllCarParks();
        assertEquals(2, carParkDtoList.size());
    }

    @Test
    void testSaveAllCarParks_When_CarParkListIsValid_Return() {
        carParkService.saveAllCarParks(carParkList);
        verify(carParkRepository, times(1)).saveAll(any());
    }

    @Test
    void testUpdateCarParkAvailability_When_CarParkAndCarParkDataIsValid_Return() {
        carParkService.updateCarParkAvailability(carPark, carParkData);
        verify(carParkRepository, times(1)).save(any());
    }

    @Test
    void testUpdateCarParkAvailability_When_CarParkAndCarParkDataIsNull_Return() {
        carParkService.updateCarParkAvailability(carPark, new CarParkData());
        verify(carParkRepository, times(0)).save(any());
    }

    @Test
    void testUpdateAllCarParksAvailability_When_CarParkAndCarParkDataIsValid_Return() {
        when(carParkRepository.findAll()).thenReturn(carParkList);
        carParkService.updateAllCarParksAvailability(carParkAvailabilityApiResponse);
        verify(carParkRepository, times(1)).save(any());
    }
}
