package com.assignment.carparkingsystem.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.service.CarParkService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ContextConfiguration(classes = {CarParkController.class})
@ExtendWith(SpringExtension.class)
class CarParkControllerTest {

  private static final String NEAREST_CAR_PARK_API = "/carparks/nearest?";
  MockHttpServletRequestBuilder requestBuilder;
  @Autowired
  private CarParkController carParkController;
  @MockBean
  private CarParkService carParkService;
  @MockBean
  private ControllerExceptionHandler controllerExceptionHandler;
  private CarParkDtoList carParkDtoList = new CarParkDtoList();

  @BeforeEach
  void setUp() {
    controllerExceptionHandler = new ControllerExceptionHandler();
    CarParkDto carParkDto = new CarParkDto("test", 1.0, 1.0, 1, 5);
    List<CarParkDto> carParkList = new ArrayList<>();
    carParkList.add(carParkDto);
    carParkDtoList = new CarParkDtoList(carParkList, 4);
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValid_Then_ReturnOk() throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=1.0");
    when(carParkService.findNearestCarParks(any())).thenReturn(carParkDtoList);
    String XTotalCountHeader = "X-Total-Count";
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.header().stringValues(XTotalCountHeader,
            String.valueOf(carParkDtoList.getTotalCarParks())));
  }

  @Test
  void testGetNearestCarParks_When_AllQueriesAreValidAndResponseIsNull_Then_ReturnNoContent()
      throws Exception {
    CarParkDtoList newCarParkDtoList = new CarParkDtoList(new ArrayList<>(), 0);
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=1.0");
    when(carParkService.findNearestCarParks(any())).thenReturn(newCarParkDtoList);
    String XTotalCountHeader = "X-Total-Count";
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder).andExpect(MockMvcResultMatchers.status().isNoContent())
        .andExpect(MockMvcResultMatchers.header().stringValues(XTotalCountHeader,
            String.valueOf(newCarParkDtoList.getTotalCarParks())));
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_LongitudeIsInvalidOrNull_Then_ReturnBadRequest(
      String longitude) throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "latitude=1.0&longitude=" + longitude);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_LatitudeIsInvalidOrNull_Then_ReturnBadRequest(String latitude)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=" + latitude);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_PageIsInvalid_Then_ReturnBadRequest(String page)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=1.0&page=" + page);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @ParameterizedTest
  @ValueSource(strings = {"null", "", "test"})
  void testGetNearestCarParks_When_PerPageIsInvalid_Then_ReturnBadRequest(String page)
      throws Exception {
    requestBuilder = MockMvcRequestBuilders.get(
        NEAREST_CAR_PARK_API + "longitude=1.0&latitude=1.0&page=1&perPage=" + page);
    MockMvcBuilders.standaloneSetup(carParkController)
        .setControllerAdvice(controllerExceptionHandler)
        .build().perform(requestBuilder)
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }
}
