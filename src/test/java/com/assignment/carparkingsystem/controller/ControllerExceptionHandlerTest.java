package com.assignment.carparkingsystem.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.assignment.carparkingsystem.constants.ErrorCode;
import com.assignment.carparkingsystem.dto.BaseErrorDto;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import java.util.List;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.BindException;

@ContextConfiguration(classes = {ControllerExceptionHandler.class})
@ExtendWith(SpringExtension.class)
class ControllerExceptionHandlerTest {

    @Autowired
    private ControllerExceptionHandler controllerExceptionHandler;

    @ParameterizedTest
    @ValueSource(strings = {
        " nested exception is java.lang.IllegalArgumentException: Invalid value double value",
        "page should be greater or equal to 1",
        "nested exception is java.lang.NumberFormatException"})
    void testHandleBindException(String exceptionMessage) {
        CarParkQueryDto carParkQueryDto = new CarParkQueryDto();
        BindException exception = new BindException(carParkQueryDto, "carParkQueryDto");
        exception.rejectValue("latitude", "400", exceptionMessage);
        ResponseEntity<List<BaseErrorDto>> response = controllerExceptionHandler.handleBindException(
            exception);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(ErrorCode.ILLEGAL_ARGUMENT, response.getBody().get(0).getCode());
        assertEquals(1, response.getBody().size());
    }
}