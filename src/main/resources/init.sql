CREATE DATABASE "car-parking-system";

\c "car-parking-system"
CREATE TABLE IF NOT EXISTS public.car_park
(
    id uuid NOT NULL,
    car_park_no character varying(255) COLLATE pg_catalog."default" NOT NULL,
    address character varying(255) COLLATE pg_catalog."default" NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    total_lots integer NOT NULL,
    available_lots integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone default CURRENT_TIMESTAMP,
    CONSTRAINT car_park_pkey PRIMARY KEY (id),
    CONSTRAINT car_park_no_uk UNIQUE (car_park_no)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.car_park
    OWNER to postgres;