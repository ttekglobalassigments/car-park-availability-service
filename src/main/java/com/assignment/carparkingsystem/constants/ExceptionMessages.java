package com.assignment.carparkingsystem.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionMessages {

    public static final String UPDATE_CAR_PARK_ERROR = "Error occurred while updating car park lot : %s";
    public static final String NUMBER_FORMAT_EXCEPTION = "Please enter a valid value for field %s";
    public static final String DATA_PARSING_ERROR = "Error while parsing data from file : %s";
    public static final String INPUT_STEAM_ERROR = "Input Stream is null";
}
