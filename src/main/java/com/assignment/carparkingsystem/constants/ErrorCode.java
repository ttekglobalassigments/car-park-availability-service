package com.assignment.carparkingsystem.constants;

public enum ErrorCode {
    ILLEGAL_ARGUMENT, BAD_DATA
}
