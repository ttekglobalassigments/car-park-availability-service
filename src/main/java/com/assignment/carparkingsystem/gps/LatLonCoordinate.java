package com.assignment.carparkingsystem.gps;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LatLonCoordinate {

    private final double latitude;
    private final double longitude;

    public SVY21Coordinate asSVY21() {
        return SVY21Conversion.computeSVY21(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LatLonCoordinate other = (LatLonCoordinate) obj;
        if (Double.doubleToLongBits(latitude) != Double
            .doubleToLongBits(other.latitude)) {
            return false;
        }
        if (Double.doubleToLongBits(longitude) != Double
            .doubleToLongBits(other.longitude)) {
            return false;
        }
        return true;
    }
}
