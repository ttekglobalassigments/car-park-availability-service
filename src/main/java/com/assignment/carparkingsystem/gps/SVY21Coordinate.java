package com.assignment.carparkingsystem.gps;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SVY21Coordinate {

    private final double easting;
    private final double northing;

    public LatLonCoordinate asLatLon() {
        return SVY21Conversion.computeLatLon(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SVY21Coordinate other = (SVY21Coordinate) obj;
        if (Double.doubleToLongBits(easting) != Double
            .doubleToLongBits(other.easting)) {
            return false;
        }
        return Double.doubleToLongBits(northing) == Double
            .doubleToLongBits(other.northing);
    }
}
