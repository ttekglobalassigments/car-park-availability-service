package com.assignment.carparkingsystem.util;

import com.assignment.carparkingsystem.gps.HaversineDistanceCalculator;
import com.assignment.carparkingsystem.gps.LatLonCoordinate;
import com.assignment.carparkingsystem.gps.SVY21Coordinate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CoordinateConversionUtility {

  private final HaversineDistanceCalculator haversineDistanceCalculator;

  /**
   * Computes SVY21 Northing and Easting based on a Latitude and Longitude coordinate.
   * <p>
   * This method returns an immutable SVY21Coordinate object that contains two fields, northing,
   * accessible with .getNorthing(), and easting, accessible with .getEasting().
   * <p>
   * This method is a shorthand for the functionally identical public SVY21Coordinate
   * computeSVY21(double lat, double lon).
   *
   * @param xCoord the x coordinate of the location.
   * @param yCoord the y coordinate of the location.
   * @return the conversion result an SVY21Coordinate.
   */
  public LatLonCoordinate convertSVY21ToLatLon(double xCoord, double yCoord) {
    SVY21Coordinate svy21Coordinate = new SVY21Coordinate(xCoord, yCoord);
    return svy21Coordinate.asLatLon();
  }

  /**
   * Caculate distance based on Haversine Formula.
   * <p>
   * This method returns a double identifying the distance between startLast, startLong, endLat,
   * endLong.
   *
   * @param startLat  starting latitude coordinate in double
   * @param startLong starting longitude coordinate in double
   * @param endLat    ending latitude coordinate in double
   * @param endLong   ending longitude coordinate in double
   * @return the calculator results the distance in double.
   */
  public double calculateHaversineDistance(double startLat, double startLong, double endLat,
      double endLong) {
    return haversineDistanceCalculator.calculateDistance(startLat, startLong, endLat, endLong);
  }

  public double setDoublePrecision(double value, int precision) {
    String formattedValue = String.format("%." + precision + "g", value);
    return Double.parseDouble(formattedValue);
  }
}
