package com.assignment.carparkingsystem.util;

import com.assignment.carparkingsystem.constants.ExceptionMessages;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.exceptions.UploadSeedDataException;
import com.assignment.carparkingsystem.gps.LatLonCoordinate;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.service.CarParkService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Slf4j
public class CarParkDataUtil {

  private final CoordinateConversionUtility coordinateConversionUtility;

  private final CarParkService carParkService;

  @Value(value = "${com.assignment.carparkingsystem.car-park-availability-api}")
  private String carParkAvailabilityApi;

  /**
   * The function will car park availability from api to get updated status of parking lots
   *
   * @return ParkAvailabilityApiResponse returns all the car park updated status
   */
  public CarParkAvailabilityApiResponse fetchCarParkAvailabilityDataFromApi() {
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate.getForObject(carParkAvailabilityApi, CarParkAvailabilityApiResponse.class);
  }

  /**
   * This function extracts seed data from csv file.
   *
   * @param fileName the name of the file which consists of car park data
   * @return list of car park data retrieved from csv file
   */
  public List<CarPark> extractCarParksFromCSVFile(String fileName) {
    // Specify the path to the CSV file containing car park data
    ClassLoader classLoader = CarParkDataUtil.class.getClassLoader();
    InputStream inputStream = classLoader.getResourceAsStream(fileName);
    if (inputStream == null) {
      log.error(ExceptionMessages.INPUT_STEAM_ERROR);
      throw new UploadSeedDataException(ExceptionMessages.INPUT_STEAM_ERROR);
    }

    // List to store car park objects
    List<CarPark> carParks = new ArrayList<>();
    // Load car park data from CSV file
    try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
      // skip parsing row header in csv file
      Stream<String> csvFileLine = br.lines().skip(1);
      csvFileLine.forEach(line -> {
        CarPark carPark = getCarPark(line);
        carParks.add(carPark);
      });
      log.info("Successfully parsed csv file data.");
    } catch (RuntimeException | IOException e) {
      log.error(String.format(ExceptionMessages.DATA_PARSING_ERROR, e));
      throw new UploadSeedDataException(
          String.format(ExceptionMessages.DATA_PARSING_ERROR, e));
    }

    return carParks;
  }

  private CarPark getCarPark(String line) {
    // Split CSV line by comma
    String[] parts = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);

    // Extract relevant information
    String carParkName = parts[0];
    String address = parts[1].replace("\"", "");
    // Convert coordinates from SVY21 to latitude and longitude
    LatLonCoordinate latLonCoordinate = coordinateConversionUtility.convertSVY21ToLatLon(
        Double.parseDouble(parts[2]),
        Double.parseDouble(parts[3]));

    // Create CarPark object and add to list
    return new CarPark(carParkName, address, latLonCoordinate);
  }
}
