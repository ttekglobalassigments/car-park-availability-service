package com.assignment.carparkingsystem.exceptions;

public class CarParkDataProcessingException extends RuntimeException{

  public CarParkDataProcessingException(String message) {
    super(message);
  }
}
