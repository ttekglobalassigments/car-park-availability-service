package com.assignment.carparkingsystem.exceptions;

public class ResourceLoadFailureException extends RuntimeException{

  public ResourceLoadFailureException(String message) {
    super(message);
  }
}
