package com.assignment.carparkingsystem.model;

import com.assignment.carparkingsystem.gps.LatLonCoordinate;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class CarPark {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(nullable = false, unique = true)
    private String carParkNo;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private double latitude;

    @Column(nullable = false)
    private double longitude;

    private int totalLots = 0;

    private int availableLots = 0;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;

    @Transient
    private double distance;

    public CarPark(String carParkNo, String address, LatLonCoordinate latLonCoordinate) {
        this.id = UUID.randomUUID();
        this.carParkNo = carParkNo;
        this.address = address;
        this.latitude = latLonCoordinate.getLatitude();
        this.longitude = latLonCoordinate.getLongitude();
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
}