package com.assignment.carparkingsystem.controller;

import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.service.CarParkService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carparks")
public class CarParkController {

    @Autowired
    private CarParkService carParkService;

    @GetMapping("/nearest")
    public ResponseEntity<List<CarParkDto>> getNearestCarParks(
        @Valid CarParkQueryDto carParkQueryDto) {
        CarParkDtoList response = carParkService.findNearestCarParks(carParkQueryDto);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("x-total-count", String.valueOf(response.getTotalCarParks()));
        return response.getCarParks().isEmpty() ? new ResponseEntity<>(responseHeaders,
            HttpStatus.NO_CONTENT)
            : new ResponseEntity<>(response.getCarParks(), responseHeaders, HttpStatus.OK);
    }
}
