package com.assignment.carparkingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CarParkingSystemApplication {

  public static void main(String[] args) {
    SpringApplication.run(CarParkingSystemApplication.class, args);
  }

}
