package com.assignment.carparkingsystem.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CarParkQueryDto {

    @JsonProperty("latitude")
    @NotNull(message = "latitude field cannot be null or empty")
    private Double latitude;

    @JsonProperty("longitude")
    @NotNull(message = "longitude field cannot be null or empty")
    private Double longitude;

    @JsonProperty("page")
    @Min(value = 1, message = "page value should be greater or equal to 1")
    private int page = 1;

    @JsonProperty("per_page")
    @Min(value = 1, message = "per page value should be greater or equal to 1")
    private int perPage = 50;
}
