package com.assignment.carparkingsystem.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarParkDtoList {

    private List<CarParkDto> carParks;
    private long totalCarParks;
}
