package com.assignment.carparkingsystem.dto;

import com.assignment.carparkingsystem.constants.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseErrorDto {

    private ErrorCode code;
    private String message;
}
