package com.assignment.carparkingsystem.seeder;

import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.service.CarParkService;
import com.assignment.carparkingsystem.util.CarParkDataUtil;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CarParkSeedDataLoader {

  @Autowired
  private CarParkService carParkService;
  @Autowired
  private CarParkDataUtil carParkDataUtil;
  @Value(value = "${com.assignment.carparkingsystem.car-park-csv-file-location}")
  private String carParkCsvFileLocation;

  @EventListener
  public void loadCarParkSeedData(ApplicationReadyEvent event) {
    try{
      if (carParkService.findAllCarParks().isEmpty()) {
        // extract car park data from csv file
        List<CarPark> carParks = carParkDataUtil.extractCarParksFromCSVFile(
            carParkCsvFileLocation);
        carParkService.saveAllCarParks(carParks);
        log.info("successfully uploaded car park seed data");
        return;
      }
      log.info("Parking Lots seed data already uploaded.");
    } catch (RuntimeException ex){
      log.error(String.format("Error occurred while loading seed data : %s", ex.getMessage()));
    }
  }
}
