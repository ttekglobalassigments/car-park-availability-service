package com.assignment.carparkingsystem.repository;

import com.assignment.carparkingsystem.model.CarPark;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarParkRepository extends CrudRepository<CarPark, UUID> {

    List<CarPark> findAll();

    Optional<CarPark> findByCarParkNo(String carParkNo);

    List<CarPark> findByAvailableLotsNot(int availableSlots);
}
