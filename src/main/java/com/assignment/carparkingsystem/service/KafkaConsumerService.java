package com.assignment.carparkingsystem.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

public interface KafkaConsumerService {

    /**
     * Accepts and performs operation to update car park details received from the kafka producer.
     *
     * @param consumerRecord consists of consumed record from kafka event sent.
     * @param acknowledgment consists of acknowledgment of kafka event.
     */
    void consumeUpdateCarParkEvent(ConsumerRecord<String, String> consumerRecord,
        Acknowledgment acknowledgment);
}
