package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkData;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import java.util.List;

public interface CarParkService {

  /**
   * Computes the nearest car parks from your coordinate.
   * <p>
   * This method returns CarParkDtoList object that contains two fields, carParks which is list of
   * car parks, and totalCarParks, which consists of total car parks received.
   *
   * @param carParkQueryDto consists of all the request parameters.
   * @return the result an CarParkDtoList.
   */
  CarParkDtoList findNearestCarParks(CarParkQueryDto carParkQueryDto);

  /**
   * Returns all car parks from your coordinate.
   * <p>
   * This method returns List of CarParkDto object which consists of all the car park details.
   *
   * @return the result is List of CarParkDto.
   */
  List<CarPark> findAllCarParks();

  /**
   * Finds car park by car park number
   *
   * @param carParkNumber consists the car park number for which information is to be fetched
   */
  CarPark findByCarParkNumber(String carParkNumber);

  /**
   * Saves or updates all car parks.
   *
   * @param carParks consists of all the car parks to be inserted or updated.
   */
  void saveAllCarParks(List<CarPark> carParks);

  /**
   * Updates all car park lot availability. The updated car park availability can be retrieved from
   * json returned from the official gov api.
   *
   * @param carParkAvailabilityApiResponse consists of all the car parks to be inserted or updated.
   */
  void updateAllCarParksAvailability(CarParkAvailabilityApiResponse carParkAvailabilityApiResponse);

  /**
   * Updates a car park lot availability. The updated value is sent in the parameter and then hence
   * updates the data and saves it in the database.
   *
   * @param carPark the car parks to be updated.
   * @param latestCarParkData the updated car park availability data.
   */
  void updateCarParkAvailability(CarPark carPark, CarParkData latestCarParkData);
}