package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.constants.ExceptionMessages;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkData;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.util.CarParkDataUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerServiceImpl implements KafkaConsumerService {

  private final CarParkService carParkService;
  private final CarParkDataUtil carParkDataUtil;

  @Override
  @KafkaListener(topics = {"${com.assignment.carparkingsystem.kafka.topic}"},
      containerFactory = "kafkaListenerContainerFactory", groupId = "car-parking")
  public void consumeUpdateCarParkEvent(ConsumerRecord<String, String> consumerRecord,
      Acknowledgment acknowledgment) {
    try {
      log.info("consumeCarParkLotUpdatedEvent received.");

      // Parse kafka string to kafka dto
      String kafkaEventDTOString = consumerRecord.value();
      CarParkData updatedCarParkData = new ObjectMapper().readValue(
          kafkaEventDTOString, CarParkData.class);

      log.debug("successfully parsed string to KafkaCarParksEventDto class object.");

      CarPark carPark = carParkService.findByCarParkNumber(updatedCarParkData.getCarParkNumber());
      if (carPark != null) {
        carParkService.updateCarParkAvailability(carPark, updatedCarParkData);
      }

      log.debug("successfully updated car park data.");
      acknowledgment.acknowledge();
    } catch (RuntimeException | JsonProcessingException ex) {
      log.error(String.format(ExceptionMessages.UPDATE_CAR_PARK_ERROR, ex));
    }
  }
}
