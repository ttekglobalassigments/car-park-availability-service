package com.assignment.carparkingsystem.service;

import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkData;
import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse.CarParkInfo;
import com.assignment.carparkingsystem.dto.CarParkDto;
import com.assignment.carparkingsystem.dto.CarParkDtoList;
import com.assignment.carparkingsystem.dto.CarParkQueryDto;
import com.assignment.carparkingsystem.model.CarPark;
import com.assignment.carparkingsystem.repository.CarParkRepository;
import com.assignment.carparkingsystem.util.CoordinateConversionUtility;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarParkServiceImpl implements CarParkService {

  private final CarParkRepository carParkRepository;

  private final CoordinateConversionUtility coordinateConversionUtility;

  @Override
  public CarParkDtoList findNearestCarParks(CarParkQueryDto carParkQueryDto) {

    List<CarPark> carParksList = carParkRepository.findAll();
    List<CarParkDto> carParkDtoList = new ArrayList<>();
    int skipCount = (carParkQueryDto.getPage() - 1) * carParkQueryDto.getPerPage();

    carParksList.stream().filter(
            carPark -> carPark.getAvailableLots() != 0) // filter out non available car park lots
        .peek(carPark -> carPark.setDistance(
            coordinateConversionUtility.calculateHaversineDistance(carParkQueryDto.getLatitude(),
                carParkQueryDto.getLongitude(), carPark.getLatitude(),
                carPark.getLongitude()))) // calculate distance between coordinates
        .sorted(Comparator.comparingDouble(
            CarPark::getDistance)) // sort car parks in ascending order based on distance
        .skip(skipCount)
        .limit(carParkQueryDto.getPerPage()) // set limited records paged on page number
        .forEach(carPark -> {
          //set latitude and longitude place values limit to 6
          carPark.setLatitude(
              coordinateConversionUtility.setDoublePrecision(carPark.getLatitude(), 6));
          carPark.setLongitude(
              coordinateConversionUtility.setDoublePrecision(carPark.getLongitude(), 6));
          carParkDtoList.add(new CarParkDto(carPark));
        });
    log.info("Received sorted and paginated car parks");
    return new CarParkDtoList(carParkDtoList, carParksList.size());
  }

  @Override
  public List<CarPark> findAllCarParks() {
    return carParkRepository.findAll();
  }

  @Override
  public CarPark findByCarParkNumber(String carParkNumber) {
    Optional<CarPark> carParkOptional = carParkRepository.findByCarParkNo(carParkNumber);
    if (carParkOptional.isPresent()) {
      return carParkOptional.get();
    }
    log.error(String.format("Car park not found for %s", carParkNumber));
    return null;
  }

  @Override
  public void saveAllCarParks(List<CarPark> carParks) {
    carParkRepository.saveAll(carParks);
  }

  @Override
  public synchronized void updateAllCarParksAvailability(
      CarParkAvailabilityApiResponse carParkAvailabilityApiResponse) {
    List<CarPark> existingCarParks = carParkRepository.findAll();

    // check that the response is not null and contains items as well
    if (Objects.nonNull(carParkAvailabilityApiResponse) && Objects.nonNull(
        carParkAvailabilityApiResponse.getItems())) {
      carParkAvailabilityApiResponse.getItems()
          .forEach(
              item -> item.getCarParkData().forEach(updatedCarParkData -> existingCarParks.stream()
                  .filter(carPark -> carPark.getCarParkNo()
                      .equals(updatedCarParkData.getCarParkNumber()))
                  .forEach(carPark -> updateCarParkAvailability(carPark, updatedCarParkData))));
    }
  }

  @Override
  public synchronized void updateCarParkAvailability(CarPark carPark,
      CarParkData latestCarParkData) {
    if (latestCarParkData != null && latestCarParkData.getCarParkInfo() != null && !latestCarParkData.getCarParkInfo().isEmpty()) {
      carPark.setAvailableLots(0);
      carPark.setTotalLots(0);

      for (CarParkInfo carParkInfo : latestCarParkData.getCarParkInfo()) {
        // calculate slots based on recent data received
        carPark.setAvailableLots(carPark.getAvailableLots() + carParkInfo.getLotsAvailable());
        carPark.setTotalLots(carPark.getTotalLots() + carParkInfo.getTotalLots());
      }
      carPark.setUpdatedAt(LocalDateTime.parse(latestCarParkData.getUpdateDatetime()));
      carParkRepository.save(carPark);
    }
  }
}
