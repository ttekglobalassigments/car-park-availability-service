package com.assignment.carparkingsystem.scheduler;

import com.assignment.carparkingsystem.dto.CarParkAvailabilityApiResponse;
import com.assignment.carparkingsystem.service.CarParkService;
import com.assignment.carparkingsystem.util.CarParkDataUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class UpdateCarParkAvailabilityScheduler {

  @Autowired
  private CarParkDataUtil carParkDataUtil;

  @Autowired
  private CarParkService carParkService;

  /**
   * This function updates car park availability status. The car park status is retrieved from the
   * api and car park data is updated.
   */
  @Scheduled(fixedDelay = 120000) // execute scheduler every 2 minutes
  public void updateCarParkAvailability() {
    try {
      CarParkAvailabilityApiResponse carParkAvailabilityApiResponse = carParkDataUtil.fetchCarParkAvailabilityDataFromApi();
      carParkService.updateAllCarParksAvailability(carParkAvailabilityApiResponse);
      log.info("Car Park availability status updated");
    } catch (RuntimeException ex) {
      log.error(String.format("Error occurred in car park scheduler: %s", ex));
    }
  }
}
