# Car Park API

This API-only application is designed to provide information about car parks closest to a user, along with each parking lot’s availability. The application is implemented in Java using the Spring Boot framework and utilizes a PostgreSQL database for storing car park information and availability data.

## Installations
* Apache Maven
* Docker
* Docker Compose

## Approach and Assumptions

* On application initialisation, the function **loadCarParkSeedData()** uploads all car parks seed data onto the database.

* In order to update car park availability status, we have added a schedular that extracts availability status from the the official gov site [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability) and updates for all car parks.

* The cron job updates the car park status in every 2 minutes.

* We have also kafka for asynchronous communication and update of the car park status. In this scenario we can only update a single car park status.

* We have used Haversine formula to calculate distance between coordinates as it seemed to be the most efficient and less commutation with respect to our requirements.

* We have used a coordinate converter class to convert SVY21 to latitude and longitude.
    
* In order to convert SVY21 to latitude and longitude format, I have used the **proj4j** library.

## Running the Application

1. Clone the Repository: Clone this repository to your local machine using Git.

```bash
git clone git@gitlab.com:ttekglobalassigments/car-park-availability-service.git
```
or
```bash
git clone https://gitlab.com/ttekglobalassigments/car-park-availability-service.git
```

2. Build the Application: Navigate to the project directory and build the application using Maven.

```bash
cd car-park-availability-service
mvn clean install
```

3. Run Docker Containers: Run docker compose file to build and run all services.

```bash
docker-compose up --build
```
### Update Car Park availability using a Kafka

4. Send a request to kafka consumer: Navigate to another terminal and run the following command from kafka producer to send request to kafka consumer

```bash
cat data.json | docker-compose exec -T kafka kafka-console-producer.sh --broker-list kafka:9092 --topic park_availability_topic
```

### Update Car Park availability using a scheduler

The system also incorporates a cron job scheduler tasked that retrieves real-time car park availability status from the official government website [car park availibity](https://api.data.gov.sg/v1/transport/carpark-availability). The scheduler is configured to update the availability status for all car parks at regular intervals of every 2 minutes.

### Fetch nearest car parks
5. Get nearest car parks: The following api will return all the nearest car parks from your location

```bash
http://localhost:8081/carparks/nearest?latitude=1.37326&longitude=103.897&page=2&perPage=3
```

##### Note: Request parameter latitude and longitude are compulsory fields while page and per page have default values 1 and 50 set respectively.

6. Shut down application: In order to shut down the application, we need to run the following command in order to close all services
```bash
docker-compose down --volumes
```

## Project Structure

- /configuration - Consists of config files like kafka consumer
- /constants - Consists of constant values used throughout the application
- /controller - Consists of all controller class and exception handler
- /dto - Includes all DTO's
- /exceptions - Includes all custom exception classes
- /gps - Consists of all custom classes for coordinate conversion and distance calculation
- /model - Includes entity models
- /repository - Includes model repository classes
- /scheduler - Includes scheduler classes
- /seeder - Consists of seeder class
- /service - Consists of service classes with business logic
- /util - Includes all utils functions used throughout the application