# For Java 11,
FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app/car-parking-system
COPY ./target/*.jar /opt/app/car-parking-system/car-parking-system.jar
ENTRYPOINT ["java","-jar","car-parking-system.jar"]